<?php
declare(strict_types=1);

namespace Soong\Task;

/**
 * Information to be passed through the task pipeline.
 */
class TaskPayload
{

    public function __construct(protected array $options)
    {
    }

    /**
     * List of runtime options to apply.
     */
    public function getOptions() : array
    {
        return $this->options;
    }
}
