<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Contracts\Task\Operation;
use Soong\Contracts\Task\Task;

abstract class TaskOperation implements Operation
{

    public function __construct(protected Task $task)
    {
    }
}
