<?php
declare(strict_types=1);

namespace Soong\Task;

use Soong\Contracts\Exception\ComponentNotFound;
use Soong\Contracts\Exception\DuplicateTask;
use Soong\Contracts\Task\Task;
use Soong\Contracts\Task\TaskContainer;

/**
 * Basic base class for migration tasks.
 */
class SimpleTaskContainer implements TaskContainer
{

    /**
     * @internal
     *
     * All known tasks, keyed by id.
     *
     * @var SimpleTask[] $tasks
     */
    protected $tasks = [];

    /**
     * @inheritdoc
     */
    public function add(string $id, Task $task) : void
    {
        if (!empty($this->tasks[$id])) {
            throw new DuplicateTask("Task $id already exists.");
        }
        $this->tasks[$id] = $task;
    }

    /**
     * @inheritdoc
     */
    public function get(string $id): Task
    {
        if (empty($this->tasks[$id])) {
            throw new ComponentNotFound("Task $id not found.");
        }
        return $this->tasks[$id];
    }

    /**
     * @inheritdoc
     */
    public function getAll(): array
    {
        return $this->tasks;
    }
}
