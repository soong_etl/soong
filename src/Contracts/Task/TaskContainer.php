<?php
declare(strict_types=1);

namespace Soong\Contracts\Task;

/**
 * Interface for managing tasks as part of a pipeline.
 */
interface TaskContainer
{

    /**
     * Retrieve the specified task.
     *
     * @param string $id
     *   ID of the task to retrieve.
     *
     *   The specified task.
     *
     * @throws \Soong\Contracts\Exception\ComponentNotFound
     *   If the specified ID is not stored in the container.
     */
    public function get(string $id) : Task;

    /**
     * Retrieve a list of all tasks.
     *
     * @return Task[]
     *   List of tasks, keyed by ID.
     */
    public function getAll() : array;

    /**
     * Add an existing task object with the given task ID.
     *
     * @param string $id
     *   ID of the task to add.
     *   Task object to add.
     *
     * @throws \Soong\Contracts\Exception\DuplicateTask
     */
    public function add(string $id, Task $task) : void;
}
