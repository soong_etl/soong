<?php

namespace Soong\Tests\Task;

use Soong\Task\SimpleTask;
use Soong\Tests\Contracts\Task\TaskTestBase;

/**
 * Tests the \Soong\Task\Task class.
 */
class TaskTest extends TaskTestBase
{

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->taskClass = '\\' . SimpleTask::class;
    }

    public function testNYI() : never
    {
        $this->markTestIncomplete('No task tests have been implemented yet.');
    }
}
