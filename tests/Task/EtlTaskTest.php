<?php

namespace Soong\Tests\Task;

use Soong\Task\SimpleEtlTask;
use Soong\Tests\Contracts\Task\EtlTaskTestBase;

/**
 * Tests the \Soong\Task\SimpleEtlTask class.
 */
class EtlTaskTest extends EtlTaskTestBase
{

    /**
     * @inheritdoc
     */
    protected function setUp() : void
    {
        parent::setUp();
        $this->taskClass = '\\' . SimpleEtlTask::class;
    }

    public function testNYI() : never
    {
        $this->markTestIncomplete('No task tests have been implemented yet.');
    }
}
