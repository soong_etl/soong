<?php

namespace Soong\Tests\Contracts\Task;

/**
 * Base class for testing EtlTask implementations.
 *
 * To test a task class, extend this class and implement setUp(), assigning
 * the fully-qualified class name to taskClass:
 *
 * @code
 *     protected function setUp() : void
 *     {
 *         $this->taskClass = '\Soong\Task\Task';
 *     }
 * @endcode
 */
abstract class EtlTaskTestBase extends TaskTestBase
{

}
