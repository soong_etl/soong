<?php

namespace Soong\Tests\Extractor;

/**
 * Tests the \Soong\Extractor\ArrayExtractor class.
 */
class UnoptimizedArrayExtractorTest extends ArrayExtractorTest
{

    /**
     * Specify the class we're testing.
     */
    protected function setUp() : void
    {
        $this->extractorClass = '\\' . UnoptimizedArrayExtractor::class;
    }
}
